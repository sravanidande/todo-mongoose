const express = require('express');
const app = express();

app.use((req, res) => {
  res.send('bye world');
});

app.listen(3000, () => console.log('listening to port 3000...'));
